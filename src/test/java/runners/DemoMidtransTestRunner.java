package runners;

import io.cucumber.testng.CucumberOptions;

@CucumberOptions(
        plugin = {"json:target/results/demomidtrans/cucumber-report.json",  "html:target/results/demomidtrans"},
        features = "src/test/resources/features",
        glue = "steps",
        tags = {"@demomidtrans"}

)
public class DemoMidtransTestRunner extends BaseTestRunner
{

}
